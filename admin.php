<!DOCTYPE html> <html lang="ru"> <head>
  <link rel="stylesheet"
			href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		<script
			src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="style.css">
    <title> Задание 1 </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width= device-width, initial-scale=1.0">
    <style>
    input[name="birthday"] {
      width: 192px;
    }
    </style>
  </head>
  <body>
    <div style="background: #BC8F9D; color: #fff;" class="header container-fluid">
      <div id="picture"><h1 style="margin: 0 auto;"> <img src="https://www.pinclipart.com/picdir/big/203-2031252_peanuts-box-clip-art-peanuts-free-engine-image.png" style="margin-right: 5px;" width = "45" alt="Кусь"></h1></div>
      <div><h1 id="site"> Let's hunt begin! </h1></div>
    </div>
<div class="container my-3 px-4" id="main">
<section style="text-align: center">
        <h2>Администрирование</h2>
    </section>
    <br><br>
    <?php if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  if (empty($_SERVER['PHP_AUTH_USER']) ||
      empty($_SERVER['PHP_AUTH_PW'])) {
      header('HTTP/1.1 401 Unanthorized');
      header('WWW-Authenticate: Basic realm="Enter login and password"');
      print('<h1>401 Требуется авторизация</h1>');
      exit();
  }

  $user = 'u41011';
  $pass = '9363823';
  $db = new PDO('mysql:host=localhost;dbname=u41011', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
  $trim_login = trim($_SERVER['PHP_AUTH_USER']);
  $trim_password = trim($_SERVER['PHP_AUTH_PW']);
  $stmtCheck = $db->prepare("SELECT * FROM admin WHERE login = ? AND password_hash = ?"); //////////////
  print(substr(hash("sha256", $trim_password), 0, 32));
  $stmtCheck -> execute([$trim_login, substr(hash("sha256", $trim_password), 0, 32)]);
  $db_response = $stmtCheck->fetch();
  
  if (empty($db_response)) {
    header('HTTP/1.1 401 Unanthorized');
    header('WWW-Authenticate: Basic realm="Invalid login or password"');
    print('<h1>401 Неверный логин или пароль</h1>');
    exit();
  }

  $stmtCount = $db->prepare('SELECT name_ability, count(fa.id_client) AS amount FROM abilities AS ab LEFT JOIN client_abilities AS fa ON ab.id_ability = fa.id_ability GROUP BY ab.id_ability');
  $stmtCount->execute();
  print('<section>');
  while($row = $stmtCount->fetch(PDO::FETCH_ASSOC)) {
    $current_sp = '';
    if ($row['name_ability'] == 'Управление временем') 
      $current_sp = 'Управление временем';
    else if ($row['name_ability'] == 'Паучье чутье') 
      $current_sp = 'Паучье чутье';
    else if ($row['name_ability'] == 'Регенерация') 
      $current_sp = 'Регенерация';
    else if ($row['name_ability'] == 'Владение магией') 
      $current_sp = 'Владение магией';
    print('<b>' . $current_sp . '</b>: ' . $row['amount'] . '<br/>');
  }
  print('</section>');

  $stmt1 = $db->prepare('SELECT id_client, name, email, birthday, gender, limbs, biography, login FROM form');
  $stmt2 = $db->prepare('SELECT id_ability FROM client_abilities WHERE id_client = ?');
  $stmt1->execute();

  while($row = $stmt1->fetch(PDO::FETCH_ASSOC)) {
      print('<br><br>');
      print('<section>');
      print('<h2>' . $row['login'] . '</h2>');
      $superpowers = [false, false, false];
      $stmt2->execute([$row['id_client']]);
      while ($super_row = $stmt2->fetch(PDO::FETCH_ASSOC)) {
          $superpowers[$super_row['id_ability'] - 1] = true;
      }
      include('user_data_form.php');
      print('</section>');
  }
} else {
  if (array_key_exists('delete', $_POST)) {
    $user = 'u41011';
    $pass = '9363823';
    $db = new PDO('mysql:host=localhost;dbname=u41011', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    $stmt1 = $db->prepare('DELETE FROM client_abilities WHERE id_client = ?');
    $stmt1->execute([$_POST['id_client']]);
    $stmt2 = $db->prepare('DELETE FROM form WHERE id_client = ?');
    $stmt2->execute([$_POST['id_client']]);
    header('Location: admin.php');
    exit();
  }

  $trimmedPost = [];
  foreach ($_POST as $key => $value)
    if (is_string($value))
      $trimmedPost[$key] = trim($value);
    else
      $trimmedPost[$key] = $value;

  if (empty($trimmedPost['name'])) {
    $hasErrors = TRUE;
  }
  $values['name'] = $trimmedPost['name'];

  if (!preg_match('/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/', $trimmedPost['email'])) {
    $hasErrors = TRUE;
  }
  $values['email'] = $trimmedPost['email'];

  if (!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $trimmedPost['birthday'])) {
    $hasErrors = TRUE;
  }
  $values['birthday'] = $trimmedPost['birthday'];

  if (!preg_match('/^[MF]$/', $trimmedPost['gender'])) {
    $hasErrors = TRUE;
  }
  $values['gender'] = $trimmedPost['gender'];

  if (!preg_match('/^[1-3]$/', $trimmedPost['limbs'])) {
    $hasErrors = TRUE;
  }
  $values['limbs'] = $trimmedPost['limbs'];

  foreach (['1', '2', '3', '4'] as $value) {
    $values['abilities'][$value] = FALSE;
  }
  if (array_key_exists('abilities', $trimmedPost)) {
    foreach ($trimmedPost['abilities'] as $value) {
      if (!preg_match('/[1-4]/', $value)) {
        $hasErrors = TRUE;
      }
      $values['abilities'][$value] = TRUE;
    }
  }
  $values['biography'] = $trimmedPost['biography'];
  

  if ($hasErrors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: admin.php');
    exit();
  }

  $user = 'u41011';
  $pass = '9363823';
  $db = new PDO('mysql:host=localhost;dbname=u41011', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
  $stmt1 = $db->prepare('UPDATE form SET name=?, email=?, birthday=?, gender=?, limbs=?, biography=? WHERE id_client = ?');
  $stmt1->execute([$values['name'], $values['email'], $values['birthday'], $values['gender'], $values['limbs'], $values['biography'], $_POST['id_client']]);

  $stmt2 = $db->prepare('DELETE FROM client_abilities WHERE id_client = ?');
  $stmt2->execute([$_POST['id_client']]);

  $stmt3 = $db->prepare("INSERT INTO client_abilities SET id_client = ?, id_ability = ?");
  foreach ($trimmedPost['abilities'] as $s)
    $stmt3 -> execute([$_POST['id_client'], $s]);

  header('Location: admin.php');
  exit();
}

?>
    </div>

    
</div>

<footer >
 <b>(c) For the Horde</b>
</footer>
  </body>
</html>
