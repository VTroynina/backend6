<?php

/**
 * Файл login.php для не авторизованного пользователя выводит форму логина.
 * При отправке формы проверяет логин/пароль и создает сессию,
 * записывает в нее логин и id пользователя.
 * После авторизации пользователь перенаправляется на главную страницу
 * для изменения ранее введенных данных.
 **/

// Отправляем браузеру правильную кодировку,
// файл login.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// Начинаем сессию.

session_start();

// В суперглобальном массиве $_SESSION хранятся переменные сессии.
// Будем сохранять туда логин после успешной авторизации.
if (!empty($_SESSION['login'])) {
  unset($_SESSION['login']);
  unset($_SESSION['id_client']);
  //setcookie('session_login', '', 1);
  //setcookie('session_uid', '', 1);
  setcookie('PHPSESSID', '', 1);
  session_destroy();
  //header('Location: ./');

  setcookie('name_error', '', 1);
  setcookie('email_error', '', 1);
  setcookie('birthday_error', '', 1);
  setcookie('gender_error', '', 1);
  setcookie('limb_number_error', '', 1);
  setcookie('superpowers_error', '', 1);
  setcookie('contract_error', '', 1);
  setcookie('name_last_value', '', 1);
  setcookie('email_last_value', '', 1);
  setcookie('birthday_last_value', '', 1);
  setcookie('gender_last_value', '', 1);
  setcookie('limb_number_last_value', '', 1);
  setcookie('sp1_last_value', '', 1);
  setcookie('sp2_last_value', '', 1);
  setcookie('sp3_last_value', '', 1);
  setcookie('sp4_last_value', '', 1);
  setcookie('biography_last_value', '', 1);

  header('Location: ./?logout');
  exit();
}


// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  include('login_fields.php');
  if(!empty($_COOKIE['login_error'])) {
    print "
        <div>
            Неверные данные для входа. Пожалуйста, повторите попытку.
    </div>";
  }
}
  // Иначе, если запрос был методом POST, т.е. нужно сделать авторизацию с записью логина в сессию.
else {
  $user = 'u41011';
  $pass = '9363823';
  $db = new PDO('mysql:host=localhost;dbname=u41011', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

  try {
    $trim_login = trim($_POST['login']);
    $trim_password = trim($_POST['password']);
    $db->beginTransaction();
    $stmt1 = $db->prepare("SELECT id_client FROM form WHERE login = ? AND password_hash = ?");
    $stmt1 -> execute([$trim_login,  hash('md5', $trim_password)]);
    $db_response = $stmt1->fetch();
    $db->commit();
  }

  catch(PDOException $e){
    print('Error : ' . $e->getMessage());
    $db->rollBack();
    exit();
  }

  if(!empty($db_response)) {
      $_SESSION['login'] = $trim_login;
      $_SESSION['id_client'] = $db_response['id_client'];
      // Записываем ID пользователя.
      session_id(substr($_POST['login'], 6, 16));
      //setcookie('session_login', $_SESSION['login'], 0);
      //setcookie('session_uid', $_SESSION['uid'], 0);

      setcookie('login_error', '', 1);
      setcookie('name_error', '', 1);
      setcookie('email_error', '', 1);
      setcookie('birthday_error', '', 1);
      setcookie('gender_error', '', 1);
      setcookie('limb_number_error', '', 1);
      setcookie('superpowers_error', '', 1);
      setcookie('contract_error', '', 1);
      setcookie('name_last_value', '', 1);
      setcookie('email_last_value', '', 1);
      setcookie('birthday_last_value', '', 1);
      setcookie('gender_last_value', '', 1);
      setcookie('limb_number_last_value', '', 1);
      setcookie('sp1_last_value', '', 1);
      setcookie('sp2_last_value', '', 1);
      setcookie('sp3_last_value', '', 1);
      setcookie('sp4_last_value', '', 1);
      setcookie('biography_last_value', '', 1);

      header('Location: ./?login_confirmed');
  }
  else  {
    header('Location: ?bad_info');
    setcookie('login_error', true, 0);
  }
}
